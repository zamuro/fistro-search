class CreateFistros < ActiveRecord::Migration[5.0]
  def change
    create_table :fistros do |t|
      t.string :select1
      t.string :select2
      t.string :text1
      t.string :text2

      t.timestamps
    end
  end
end
