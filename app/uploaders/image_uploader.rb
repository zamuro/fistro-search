class ImageUploader < CarrierWave::Uploader::Base
  alias_method :extension_white_list, :extension_whitelist
  include CarrierWaveDirect::Uploader
  # storage :file
  storage :fog

  # def store_dir
  #   "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  # end
end
