class Torpedo < ApplicationRecord
  mount_uploader :image, ImageUploader
  after_save :enqueue_image

  def image_name
    File.basename(image.path || image.filename) if image
  end

  def enqueue_image
    ImageWorker.perform_async(id, key) if key.present?
  end

  class ImageWorker
    include Sidekiq::Worker

    def perform(id,key)
      torpedo = Torpedo.find(id)
      torpedo.key = key
      torpedo.remote_image_url = torpedo.image.direct_fog_url(with_path: true)
      torpedo.save!
    end
  end
end
