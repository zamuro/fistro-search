json.extract! torpedo, :id, :name, :img, :created_at, :updated_at
json.url torpedo_url(torpedo, format: :json)
