module FistrosHelper
  def select_fistro_1
    fistro = Fistro.select(:select1).where.not(:select1 => nil)
    fistro.map.each do |camarerorl|
      camarerorl.select1.to_s
    end
  end
  def select_fistro_2
    fistro = Fistro.select(:select2).where.not(:select2 => nil)
    fistro.map.each do |camarerorl|
      camarerorl.select2.to_s
    end
  end
end
