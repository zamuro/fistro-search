class TorpedosController < ApplicationController
  before_action :set_torpedo, only: [:show, :edit, :update, :destroy]

  # GET /torpedos
  # GET /torpedos.json
  def index
    @torpedos = Torpedo.all
    @benemeritar = Torpedo.new.image
    @benemeritar.success_action_redirect = new_torpedo_url
  end

  # GET /torpedos/1
  # GET /torpedos/1.json
  def show
  end

  # GET /torpedos/new
  def new
    @torpedo = Torpedo.new(key: params[:key])
  end

  # GET /torpedos/1/edit
  def edit
  end

  # POST /torpedos
  # POST /torpedos.json
  def create
    @torpedo = Torpedo.new(torpedo_params)

    respond_to do |format|
      if @torpedo.save
        format.html { redirect_to @torpedo, notice: 'Torpedo was successfully created.' }
        format.json { render :show, status: :created, location: @torpedo }
      else
        format.html { render :new }
        format.json { render json: @torpedo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /torpedos/1
  # PATCH/PUT /torpedos/1.json
  def update
    respond_to do |format|
      if @torpedo.update(torpedo_params)
        format.html { redirect_to @torpedo, notice: 'Torpedo was successfully updated.' }
        format.json { render :show, status: :ok, location: @torpedo }
      else
        format.html { render :edit }
        format.json { render json: @torpedo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /torpedos/1
  # DELETE /torpedos/1.json
  def destroy
    @torpedo.destroy
    respond_to do |format|
      format.html { redirect_to torpedos_url, notice: 'Torpedo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_torpedo
      @torpedo = Torpedo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def torpedo_params
      params.require(:torpedo).permit(:name, :image, :remote_image_url, :key)
    end
end
