class FistrosController < ApplicationController

  def show
    @fistro = Fistro.find(params[:id])
  end

  def index
    @fistro = Fistro.new
    @fistros ||= fistro_search
  end

  def create
    @fistro = Fistro.new(fistro_params)
    if @fistro.save
      redirect_to @fistro
    else
      render 'new'
    end
  end

  def edit
    @fistro = Fistro.find(params[:id])
  end

  private

    def fistro_params
      params.require(:fistro).permit(:select1, :select2,:text2)
    end

    def fistro_search
      fistro = Fistro.all
      fistro = Fistro.where(:select1 => params[:select1]) if params[:select1].present?
      fistro = Fistro.where(:select2 => params[:select2]) if params[:select2].present?
      fistro = Fistro.where(:text2 => params[:text_from]..params[:text_to]) if params[:text_to].present?
      fistro
    end

  end