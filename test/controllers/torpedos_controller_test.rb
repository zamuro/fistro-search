require 'test_helper'

class TorpedosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @torpedo = torpedos(:one)
  end

  test "should get index" do
    get torpedos_url
    assert_response :success
  end

  test "should get new" do
    get new_torpedo_url
    assert_response :success
  end

  test "should create torpedo" do
    assert_difference('Torpedo.count') do
      post torpedos_url, params: { torpedo: { img: @torpedo.img, name: @torpedo.name } }
    end

    assert_redirected_to torpedo_url(Torpedo.last)
  end

  test "should show torpedo" do
    get torpedo_url(@torpedo)
    assert_response :success
  end

  test "should get edit" do
    get edit_torpedo_url(@torpedo)
    assert_response :success
  end

  test "should update torpedo" do
    patch torpedo_url(@torpedo), params: { torpedo: { img: @torpedo.img, name: @torpedo.name } }
    assert_redirected_to torpedo_url(@torpedo)
  end

  test "should destroy torpedo" do
    assert_difference('Torpedo.count', -1) do
      delete torpedo_url(@torpedo)
    end

    assert_redirected_to torpedos_url
  end
end
