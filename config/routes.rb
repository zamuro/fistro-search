Rails.application.routes.draw do
  resources :torpedos
  resources :fistros
  root :to => 'fistros#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
